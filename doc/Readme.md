# Documentation

**Theoretical part** 📖 of the developed project 🤓.

I used Draw.io to create [this diagram](Finance.drawio)

## ER Diagram

![Diagrama ER](Finance.svg)

## Relational

![Relational](finance_app.png)
by **DataGrip**
