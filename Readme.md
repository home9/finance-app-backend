# Finance Backend 💸💰

Visit my [webpage](https://jsiapo.dev) 🌎

**Backend** built with [**HapiJS**](https://hapi.dev/) framework ⚙️ for [Mobile App](https://gitlab.com/home9/finance-app) 📱 build with Flutter

## Develop Environment 🚧

1. **Create** [.env](https://gitlab.com/JSiapo/finanace-app-backend/-/snippets/2083323) file 📜

2. **Install depenendcies** `npm i`

3. **Run in develop** `npm run dev`

## Production Environment 🚀

1. **Create** [.env](https://gitlab.com/JSiapo/finanace-app-backend/-/snippets/2083323) file 📜

2. **Build with** `npm run build`

3. **Run** `npm run start`

4. If using **PM2** `pm2 start --name "finance" npm -- start`
