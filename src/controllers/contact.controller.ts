import { Request, ResponseToolkit } from '@hapi/hapi';
import Boom from '@hapi/boom';

import {
  // findContactByNameInteractor,
  findContactInteractor,
  listContactInteractor,
  registerContactInteractor,
  removeContactInteractor,
  updateContactInteractor,
} from '@core/interactors/contact';

import { getUserByIdInteractor } from '@core/interactors/user';
import { GlobalUser } from '@/internals/shared';
import { Contact } from '@/core/entities';
import { validateEntity } from '@/internals/utils';

export const list = async (_: Request, res: ResponseToolkit) => {
  try {
    const { id } = res.request.auth.credentials as unknown as { id: string };
    const user = await getUserByIdInteractor(id);
    if (!user) return Boom.notFound('Usuario no encontrado');
    const [contacts, count] = await listContactInteractor({ user });

    return res.response({
      result: contacts.map((contact) => {
        return { ...contact };
      }),
      total: count,
    });
  } catch (error: any) {
    return Boom.badRequest(error.message ?? error);
  }
};

export const find = async (req: Request, res: ResponseToolkit) => {
  try {
    const { id } = (await GlobalUser.getUser(req)) as { id: string };
    const user = await getUserByIdInteractor(id);
    if (!user) return Boom.notFound('Usuario no encontrado');
    const contact = await findContactInteractor({ id, user });
    return res.response({ ...contact });
  } catch (error: any) {
    return Boom.badRequest(error.message ?? error);
  }
};
export const register = async (req: Request, res: ResponseToolkit) => {
  try {
    const { id } = (await GlobalUser.getUser(req)) as { id: string };
    const user = await getUserByIdInteractor(id);
    if (!user) return Boom.notFound('Usuario no encontrado');
    const { name, nro, nroCCI } = req.payload as unknown as {
      name: string;
      nro: string;
      nroCCI: string;
    };
    const contact = new Contact();
    contact.name = name;
    contact.nro = nro;
    contact.nroCCI = nroCCI;

    const invalidContact = await validateEntity(contact);
    if (invalidContact) {
      const error = Boom.badRequest('Debe enviar correctamente todos campos');
      error.reformat();
      error.output.payload.invalid = invalidContact; // Add custom key
      return error;
    }
    const contactRegister = await registerContactInteractor({ contact, user });
    // DeepBuffer(contactRegister);

    return res.response({
      message: 'Contacto creado correctamente',
      contact: contactRegister,
    });
  } catch (error: any) {
    return Boom.badRequest(error.message ?? error);
  }
};

export const update = async (req: Request, res: ResponseToolkit) => {
  try {
    const { id } = (await GlobalUser.getUser(req)) as { id: string };
    const user = await getUserByIdInteractor(id);
    if (!user) return Boom.notFound('Usuario no encontrado');
    const { name, nro, nroCCI } = req.payload as unknown as {
      name?: string;
      nro?: string;
      nroCCI?: string;
    };
    const contact = await findContactInteractor({ id, user });
    if (!contact) return Boom.notFound('Contacto no encontrado');
    contact.name = name ?? contact.name;
    contact.nro = nro ?? contact.nro;
    contact.nroCCI = nroCCI ?? contact.nroCCI;
    const invalidContact = await validateEntity(contact);
    if (invalidContact) {
      const error = Boom.badRequest('Debe enviar correctamente todos campos');
      error.reformat();
      error.output.payload.invalid = invalidContact; // Add custom key
      return error;
    }
    return res.response({
      message: 'Contacto actualizado correctamente',
      contact: await updateContactInteractor(contact),
    });
  } catch (error: any) {
    return Boom.badRequest(error.message ?? error);
  }
};

export const remove = async (req: Request, res: ResponseToolkit) => {
  try {
    const { id } = (await GlobalUser.getUser(req)) as { id: string };
    const user = await getUserByIdInteractor(id);
    if (!user) return Boom.notFound('Usuario no encontrado');

    const contact = await findContactInteractor({ id, user });
    if (!contact) return Boom.notFound('Contacto no encontrado');

    return res.response({
      message: 'Eliminado correctamente',
      contact: await removeContactInteractor(contact),
    });
  } catch (error: any) {
    return Boom.badRequest(error.message ?? error);
  }
};
