import { Request, ResponseToolkit } from '@hapi/hapi';
import Boom from '@hapi/boom';

import { Params, Query } from '@internals/shared';
import {
  getActiveUsersInteractor,
  createUserInteractor,
  getUserByUidInteractor,
  deleteUserInteractor,
  getUserByIdInteractor,
  updateUserInteractor,
} from '@core/interactors/user';

import {
  validateEntity,
  Firebase,
  hateoas,
  Date,
  Token,
  email,
  encrypt,
  checkPassword,
} from '@internals/utils';
import { getClientIp } from '@supercharge/request-ip';
// import { EUserRegister, User } from '@/core/entities';
import { NODE_ENV } from '@/config';
import {
  getUserIpInteractor,
  registerIpInteractor,
} from '@/core/interactors/userIp';
import { UserIpAllow } from '@/core/entities';

type Body = {
  uid: string;
  password?: string;
  fullName?: string;
  email?: string;
  photo?: string;
  pin: string;
  device: string;
};

export const isUserRegister = async (req: Request, res: ResponseToolkit) => {
  try {
    const { uid } = req.payload as Body;
    if (await getUserByUidInteractor(uid))
      return res.response({ message: 'Usuario está registrado' }).code(200);
    return Boom.notFound('No se encuentra registrado');
  } catch (error: any) {
    return Boom.badRequest(error.message ?? error);
  }
};

//TODO send master key
export const createUser = async (req: Request, res: ResponseToolkit) => {
  try {
    const payload = req.payload as Body;

    const location = (req as any).location;
    if (!location.city)
      return Boom.preconditionFailed('No se encontró localización');

    const { uid, fullName, pin, device } = payload;
    if (!pin || !device || !uid)
      return Boom.unauthorized('Debe enviar su PIN y dispositivo / usuario');

    const ipClient = getClientIp(req);
    if (!ipClient) return Boom.badData('No se pudo obtener el ip');

    if (!uid) return Boom.badData('Debe registrar con google');
    const userFound = await getUserByUidInteractor(uid);
    if (userFound) return Boom.preconditionFailed('Usuario ya registrado');

    //Get user from Firebase
    const firebase = new Firebase();
    firebase.userid = uid;
    const user = await firebase.getUserFromFirebase({ fullName });

    if (!user) return Boom.notFound('Usuario no encontrado');
    user.pin = pin;
    const invalidUser = await validateEntity(user);
    if (invalidUser) {
      const error = Boom.badRequest('Debe enviar correctamente todos campos');
      error.reformat();
      error.output.payload.invalid = invalidUser; // Add custom key
      return error;
    }
    user.pin = await encrypt(user.pin);
    const userSaved = await createUserInteractor(user);

    const userIp = new UserIpAllow();
    userIp.user = userSaved;
    userIp.device = device;
    userIp.ip = ipClient;

    if (invalidUser) {
      const error = Boom.badRequest(
        'Debe enviar correctamente todos los campos'
      );
      error.reformat();
      error.output.payload.invalid = invalidUser; // Add custom key
      return error;
    }
    const userIpAllow = await registerIpInteractor(userIp);
    const token = new Token();

    //TODO update ip with id ipALlow
    token.encode({
      ip: userIpAllow.id,
      id: userSaved.id,
      email: userSaved.email,
      scope: ['user'],
    });
    return res
      .response({
        message: 'Usuario Registrado',
        user: userSaved,
        token: token.token,
      })
      .code(201);
  } catch (error: any) {
    return Boom.badRequest(error.message ?? error);
  }
};

export const getUserById = async (req: Request, res: ResponseToolkit) => {
  try {
    const params = req.params as Params;
    const { id } = params;
    const userFound = await getUserByIdInteractor(id);
    if (!userFound) return Boom.notFound('No se encontró el usuario');
    return res.response({ ...userFound });
  } catch (error: any) {
    return Boom.badRequest(error.message ?? error);
  }
};

export const getUsers = async (req: Request, res: ResponseToolkit) => {
  try {
    const query = req.query as Query;
    const { limit, page } = query;
    const [users, count] = await getActiveUsersInteractor({
      limit: limit ?? 10,
      page: page ?? 0,
    });

    const hateoasUsers = hateoas(users, { limit, page, count });

    return res.response({
      ...hateoasUsers,
      total: count,
    });
  } catch (error: any) {
    return Boom.badRequest(error.message ?? error);
  }
};

export const deleteUser = async (req: Request, res: ResponseToolkit) => {
  try {
    const params = req.params as Params;
    const { id } = params;
    return res
      .response({
        message: 'Usuario eliminado',
        reult: await deleteUserInteractor(id),
      })
      .code(200);
  } catch (error: any) {
    return Boom.badRequest(error.message ?? error);
  }
};

export const udpateUser = async (req: Request, res: ResponseToolkit) => {
  try {
    const params = req.params as Params;
    const payload = req.payload as Body;
    const { id } = params;

    const { fullName, email, photo, pin } = payload;

    const userFound = await getUserByIdInteractor(id);
    if (!userFound) return Boom.notFound('No se encontró el usuario');

    fullName && (userFound.fullName = fullName);
    email && (userFound.email = email);
    photo && (userFound.photo = photo);
    pin && (userFound.pin = pin);

    const invalidUser = await validateEntity(userFound);
    if (invalidUser) {
      const error = Boom.badRequest('Debe enviar correctamente todos campos');
      error.reformat();
      error.output.payload.invalid = invalidUser; // Add custom key
      return error;
    }

    pin && (userFound.pin = await encrypt(userFound.pin));
    return res
      .response({
        message: 'Usuario Actualizado',
        user: await updateUserInteractor(userFound),
      })
      .code(201);
  } catch (error: any) {
    return Boom.badRequest(error.message ?? error);
  }
};

export const login = async (req: Request, res: ResponseToolkit) => {
  try {
    const location = (req as any).location;
    if (!location.city)
      return Boom.preconditionFailed('No se encontró localización');
    const { uid, pin } = req.payload as Body;
    const userFound = await getUserByUidInteractor(uid);

    if (!userFound) return Boom.notFound('Usuario no registrado');
    const ip = getClientIp(req);
    if (!(await checkPassword(pin, userFound.pin)))
      return Boom.unauthorized('PIN no válido');
    const ipAllow = await getUserIpInteractor({
      user: userFound,
      ip: ip ?? '',
    });
    if (!ipAllow) return Boom.unauthorized('IP no autorizada');

    const date = new Date();

    userFound.lastLogin = date.nowUTC('YYYY-MM-DD HH:mm:ss');
    userFound.lastIp = location?.ip;
    userFound.lastCity = location?.city ?? userFound.lastCity;
    userFound.lastRegion = location?.region ?? userFound.lastRegion;
    userFound.lastCountry = location?.country ?? userFound.lastCountry;

    NODE_ENV === 'PRODUCTION' &&
      (await email({
        to: userFound.email,
        ...location,
      }));

    const user = await updateUserInteractor(userFound);

    const token = new Token();

    // const ipAllow =
    token.encode({
      ip: ipAllow.id,
      id: user.id,
      email: user.email,
      scope: ['user'],
    });

    return res.response({ user, token: token.token });
  } catch (error: any) {
    return Boom.badRequest(error.message ?? error);
  }
};
