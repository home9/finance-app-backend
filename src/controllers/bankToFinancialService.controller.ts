import { Request, ResponseToolkit } from '@hapi/hapi';
import Boom from '@hapi/boom';

import { findFinancialInteractor } from '@core/interactors/financialService';
import { findBankInteractor } from '@core/interactors/bank';
import {
  registerBankToFinancialServiceInteractor,
  deleteBankToFinancialServiceInteractor,
  findBankToFinancialServiceInteractor,
  listBankToFinancialServiceInteractor,
  updateBankToFinancialServiceInteractor,
} from '@core/interactors/bankToFinancialService';

import { BankToFinancialService, ECardType } from '@core/entities';
import { validateEntity } from '@/internals/utils';

export const register = async (req: Request, res: ResponseToolkit) => {
  try {
    const {
      bank: bankId,
      financial_service: financialServiceId,
      card,
      design,
    } = req.payload as unknown as {
      bank: string;
      financial_service: string;
      card: ECardType;
      design: number;
    };
    const bank = await findBankInteractor(bankId);
    if (!bank) return Boom.notFound('No se encontró el banco');
    const financialService = await findFinancialInteractor(financialServiceId);
    if (!financialService)
      return Boom.notFound('No se encontró el servicio financiero');
    if (!(card in ECardType))
      return Boom.notFound('Debe enviar un tipo de tarjeta correcto');
    const bankToFinancialService = new BankToFinancialService();

    bankToFinancialService.bank = bank;
    bankToFinancialService.financialServices = financialService;
    bankToFinancialService.cardType =
      ECardType[card as unknown as keyof typeof ECardType];
    bankToFinancialService.design = Number(design);
    const invalidBankToFinancialService = await validateEntity(
      bankToFinancialService
    );
    if (invalidBankToFinancialService) {
      const error = Boom.badRequest('Debe enviar correctamente todos campos');
      error.reformat();
      error.output.payload.invalid = invalidBankToFinancialService; // Add custom key
      return error;
    }

    return res.response({
      message: 'Servicio con banco registrado correctamente',
      bank_service: {
        ...(await registerBankToFinancialServiceInteractor(
          bankToFinancialService
        )),
      },
    });
  } catch (error: any) {
    return Boom.badRequest(error.message ?? error);
  }
};

export const list = async (_: Request, res: ResponseToolkit) => {
  try {
    const [bankfinancialServices, count] =
      await listBankToFinancialServiceInteractor();
    return res.response({
      result: bankfinancialServices.map((bankfinancialService) => {
        return { ...bankfinancialService };
      }),
      total: count,
    });
  } catch (error: any) {
    return Boom.badRequest(error.message ?? error);
  }
};

export const find = async (req: Request, res: ResponseToolkit) => {
  try {
    const params = req.params as unknown as { id: string };
    const { id } = params;
    const bankToFinancialService = await findBankToFinancialServiceInteractor(
      id
    );
    if (!bankToFinancialService)
      return Boom.notFound('No se encontró el banco con servicio');
    return res.response({ ...bankToFinancialService });
  } catch (error: any) {
    return Boom.badRequest(error.message ?? error);
  }
};

export const update = async (req: Request, res: ResponseToolkit) => {
  try {
    const {
      bank: bankId,
      financial_service: financialServiceId,
      card,
      design,
    } = req.payload as unknown as {
      bank: string;
      financial_service: string;
      card: ECardType;
      design: number;
    };

    const { id } = req.params as unknown as { id: string };

    const bankFinancialFound = await findBankToFinancialServiceInteractor(id);

    if (!bankFinancialFound)
      return Boom.notFound('No se encontró el servicio financiero');

    if (bankId) {
      const bank = await findBankInteractor(bankId);
      if (!bank) return Boom.notFound('No se encontró el servicio financiero');
      bankFinancialFound.bank = bank;
    }
    if (financialServiceId) {
      const financialService = await findFinancialInteractor(
        financialServiceId
      );
      if (!financialService)
        return Boom.notFound('No se encontró el servicio financiero');
      bankFinancialFound.financialServices = financialService;
    }
    if (card) {
      if (!(card in ECardType))
        return Boom.notFound('Debe enviar un tipo de tarjeta correcto');
      bankFinancialFound.cardType =
        ECardType[card as unknown as keyof typeof ECardType];
    }
    if (design) {
      bankFinancialFound.design = Number(design);
    }

    const invalidBankFinancialService = await validateEntity(
      bankFinancialFound
    );
    if (invalidBankFinancialService) {
      const error = Boom.badRequest('Debe enviar correctamente todos campos');
      error.reformat();
      error.output.payload.invalid = invalidBankFinancialService; // Add custom key
      return error;
    }

    const bankFinancialUpdated = await updateBankToFinancialServiceInteractor(
      bankFinancialFound
    );

    return res
      .response({
        message: 'Serivicio Financiero Actualizado',
        bank: bankFinancialUpdated,
      })
      .code(200);
  } catch (error: any) {
    return Boom.badRequest(error.message ?? error);
  }
};

export const remove = async (req: Request, res: ResponseToolkit) => {
  try {
    const params = req.params as unknown as { id: string };
    const { id } = params;
    const bankFinancial = await findBankToFinancialServiceInteractor(id);
    if (!bankFinancial)
      return Boom.notFound('No se encontró el banco con servicio financiero');
    const bankFinancialDeleted = await deleteBankToFinancialServiceInteractor(
      bankFinancial
    );
    return res.response({
      bank: bankFinancialDeleted,
      message: 'Eliminado correctamente',
    });
  } catch (error: any) {
    return Boom.badRequest(error.message ?? error);
  }
};
