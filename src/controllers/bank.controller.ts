import { Request, ResponseToolkit } from '@hapi/hapi';
import Boom from '@hapi/boom';

import {
  deleteBankInteractor,
  findBankInteractor,
  listBanksInteractor,
  registerBankInteractor,
  updateBankInteractor,
  findBankByNameInteractor,
} from '@core/interactors/bank';

import { Bank } from '@core/entities';

import { validateEntity } from '@internals/utils';

type Body = {
  name?: string;
  isBank?: boolean;
};

type Params = {
  id: string;
};

export const findBank = async (req: Request, res: ResponseToolkit) => {
  try {
    const params = req.params as Params;
    const { id } = params;
    const bank = await findBankInteractor(id);
    if (!bank)
      return res.response({ message: 'No se encontró el banco' }).code(404);
    return res.response({ ...bank });
  } catch (error: any) {
    return Boom.badRequest(error.message ?? error);
  }
};
export const listBank = async (_: Request, res: ResponseToolkit) => {
  try {
    const [banks, count] = await listBanksInteractor();
    return res.response({
      result: banks.map((bank) => {
        return { ...bank };
      }),
      total: count,
    });
  } catch (error: any) {
    return Boom.badRequest(error.message ?? error);
  }
};
export const registerBank = async (req: Request, res: ResponseToolkit) => {
  try {
    const payload = req.payload as Body;
    const { name, isBank } = payload;
    if (!name || isBank == undefined) {
      return res.response({ message: 'Debe enviar todos los datos' }).code(400);
    }
    const bankFound = await findBankByNameInteractor(name);
    if (bankFound) return Boom.badData('Banco ya registrado');

    const bank = new Bank();
    bank.name = name;
    bank.isBank = isBank;

    const invalidBank = await validateEntity(bank);
    if (invalidBank) {
      const error = Boom.badRequest('Debe enviar correctamente todos campos');
      error.reformat();
      error.output.payload.invalid = invalidBank; // Add custom key
      return error;
    }

    const bankRegister = await registerBankInteractor(bank);
    return res
      .response({
        message: 'Banco Registrado',
        bank: { ...bankRegister },
      })
      .code(201);
  } catch (error: any) {
    return Boom.badRequest(error.message ?? error);
  }
};
export const updateBank = async (req: Request, res: ResponseToolkit) => {
  try {
    const payload = req.payload as Body;
    const { name, isBank } = payload;

    const params = req.params as Params;
    const { id } = params;
    const bank = await findBankInteractor(id);

    if (!bank) return Boom.notFound('No se encontró el banco');

    bank.isBank = isBank ?? bank.isBank;
    bank.name = name ?? bank.name;

    const invalidBank = await validateEntity(bank);
    if (invalidBank) {
      const error = Boom.badRequest('Debe enviar correctamente todos campos');
      error.reformat();
      error.output.payload.invalid = invalidBank; // Add custom key
      return error;
    }

    const bankUpdated = await updateBankInteractor(bank);

    return res
      .response({
        message: 'Banco Actualizado',
        bank: bankUpdated,
      })
      .code(200);
  } catch (error: any) {
    return Boom.badRequest(error.message ?? error);
  }
};

export const deleteBank = async (req: Request, res: ResponseToolkit) => {
  try {
    const params = req.params as Params;
    const { id } = params;
    const bank = await findBankInteractor(id);
    if (!bank) return Boom.notFound('No se encontró el banco');
    const bankDeleted = await deleteBankInteractor(bank);
    return res.response({
      bank: bankDeleted,
      message: 'Eliminado correctamente',
    });
  } catch (error: any) {
    return Boom.badRequest(error.message ?? error);
  }
};
