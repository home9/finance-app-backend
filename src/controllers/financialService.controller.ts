import { Request, ResponseToolkit } from '@hapi/hapi';
import Boom from '@hapi/boom';

import {
  deleteFinancialInteractor,
  findByNameFinancialInteractor,
  findFinancialInteractor,
  listFinancialInteractor,
  registerFinancialInteractor,
  updateFinancialInteractor,
} from '@core/interactors/financialService';

import { FinancialService } from '@core/entities';
import { validateEntity } from '@/internals/utils';

export const register = async (req: Request, res: ResponseToolkit) => {
  try {
    const { name, icon } = req.payload as unknown as {
      name: string;
      icon: string;
    };
    const financialServiceFound = await findByNameFinancialInteractor(name);
    if (financialServiceFound)
      return Boom.badData('Servicio Financiero ya registrado');
    const financialService = new FinancialService();
    financialService.name = name;
    financialService.icon = icon;
    const invalidFinancialService = await validateEntity(financialService);
    if (invalidFinancialService) {
      const error = Boom.badRequest('Debe enviar correctamente todos campos');
      error.reformat();
      error.output.payload.invalid = invalidFinancialService; // Add custom key
      return error;
    }
    return res.response({
      message: 'Servicio registrado correctamente',
      service: { ...(await registerFinancialInteractor(financialService)) },
    });
  } catch (error: any) {
    return Boom.badRequest(error.message ?? error);
  }
};

export const list = async (_: Request, res: ResponseToolkit) => {
  try {
    const [financialServices, count] = await listFinancialInteractor();
    return res.response({
      result: financialServices.map((financialService) => {
        return { ...financialService };
      }),
      total: count,
    });
  } catch (error: any) {
    return Boom.badRequest(error.message ?? error);
  }
};

export const find = async (req: Request, res: ResponseToolkit) => {
  try {
    const params = req.params as unknown as { id: string };
    const { id } = params;
    const financial = await findFinancialInteractor(id);
    if (!financial) return Boom.notFound('No se encontró servicio financiero');
    return res.response({ ...financial });
  } catch (error: any) {
    return Boom.badRequest(error.message ?? error);
  }
};

export const update = async (req: Request, res: ResponseToolkit) => {
  try {
    const payload = req.payload as unknown;
    const { name, icon } = payload as {
      name: string;
      icon: string;
    };

    const params = req.params as unknown as { id: string };
    const { id } = params;
    const financial = await findFinancialInteractor(id);

    if (!financial)
      return Boom.notFound('No se encontró el servicio financiero');

    financial.name = name ?? financial.name;
    financial.icon = icon ?? financial.icon;

    const invalidFinancialService = await validateEntity(financial);
    if (invalidFinancialService) {
      const error = Boom.badRequest('Debe enviar correctamente todos campos');
      error.reformat();
      error.output.payload.invalid = invalidFinancialService; // Add custom key
      return error;
    }

    const bankUpdated = await updateFinancialInteractor(financial);

    return res
      .response({
        message: 'Serivicio Financiero Actualizado',
        bank: bankUpdated,
      })
      .code(200);
  } catch (error: any) {
    return Boom.badRequest(error.message ?? error);
  }
};

export const remove = async (req: Request, res: ResponseToolkit) => {
  try {
    const params = req.params as unknown as { id: string };
    const { id } = params;
    const financial = await findFinancialInteractor(id);
    if (!financial)
      return Boom.notFound('No se encontró el servicio financiero');
    const financialDeleted = await deleteFinancialInteractor(financial);
    return res.response({
      bank: financialDeleted,
      message: 'Eliminado correctamente',
    });
  } catch (error: any) {
    return Boom.badRequest(error.message ?? error);
  }
};
