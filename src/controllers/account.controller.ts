// //isActive false user has account where account is deleted
import { Request, ResponseToolkit } from '@hapi/hapi';
import Boom from '@hapi/boom';
import { findBankToFinancialServiceInteractor } from '@core/interactors/bankToFinancialService';
import {
  registerAccountInteractor,
  findAccountInteractor,
  listAccountInteractor,
  updateAccountInteractor,
  deleteAccountInteractor,
} from '@core/interactors/account';

import { Account } from '@core/entities';
import { validateEntity } from '@/internals/utils';
import { ETypeCurrency } from '@/constants/typeCurrency';

export const list = async (_: Request, res: ResponseToolkit) => {
  try {
    const [accounts, count] = await listAccountInteractor();
    return res.response({
      result: accounts.map((account) => {
        return { ...account };
      }),
      total: count,
    });
  } catch (error: any) {
    return Boom.badRequest(error.message ?? error);
  }
};

export const find = async (req: Request, res: ResponseToolkit) => {
  try {
    const params = req.params as unknown as { id: string };
    const { id } = params;
    const account = await findAccountInteractor(id);
    if (!account) return Boom.notFound('No se encontró el banco con servicio');
    return res.response({ ...account });
  } catch (error: any) {
    return Boom.badRequest(error.message ?? error);
  }
};

export const register = async (req: Request, res: ResponseToolkit) => {
  try {
    const {
      name,
      nro,
      nroCCI,
      ammount = 0,
      typeCurrency = 'pen',
      description = '',
      bankToFinancialService,
    } = req.payload as unknown as {
      name: string;
      ammount: number;
      typeCurrency: string;
      nro: string;
      nroCCI: string;
      description: string;
      bankToFinancialService: string;
    };
    const account = new Account();
    account.name = name;
    account.nro = nro;
    account.nroCCI = nroCCI;
    account.description ??= description;
    account.amount = ammount;
    account.typeCurrency =
      typeCurrency === 'pen' ? ETypeCurrency.PEN : ETypeCurrency.USD;
    const invalidAccount = await validateEntity(account);
    if (invalidAccount) {
      const error = Boom.badRequest('Debe enviar correctamente todos campos');
      error.reformat();
      error.output.payload.invalid = invalidAccount; // Add custom key
      return error;
    }

    return res.response({
      message: 'Cuenta creada exitosamente',
      result: {
        ...(await registerAccountInteractor(account, bankToFinancialService)),
      },
    });
  } catch (error: any) {
    return Boom.badRequest(error.message ?? error);
  }
};

export const update = async (req: Request, res: ResponseToolkit) => {
  try {
    const params = req.params as unknown as { id: string };
    const { id } = params;
    const account = await findAccountInteractor(id);
    if (!account) return Boom.notFound('No se encontró la cuenta');

    const {
      name,
      nro,
      nroCCI,
      ammount,
      typeCurrency,
      description,
      bankToFinancialService,
    } = req.payload as unknown as {
      name?: string;
      ammount?: number;
      typeCurrency?: string;
      nro?: string;
      nroCCI?: string;
      description?: string;
      bankToFinancialService?: string;
    };

    name && (account.name = name);
    nro && (account.nro = nro);
    nroCCI && (account.nroCCI = nroCCI);
    ammount && (account.amount = ammount);
    typeCurrency &&
      (account.typeCurrency =
        typeCurrency === 'pen' ? ETypeCurrency.PEN : ETypeCurrency.USD);
    description && (account.description = description);
    if (bankToFinancialService) {
      const bankToFinancialServiceFound =
        await findBankToFinancialServiceInteractor(bankToFinancialService);
      if (!bankToFinancialServiceFound) {
        return Boom.notFound('No se encontró el banco con servicio');
      }
      account.bankToFinancialService = bankToFinancialServiceFound;
    }
    const invalidAccount = await validateEntity(account);
    if (invalidAccount) {
      const error = Boom.badRequest('Debe enviar correctamente todos campos');
      error.reformat();
      error.output.payload.invalid = invalidAccount; // Add custom key
      return error;
    }
    const accountUpdated = await updateAccountInteractor(account);
    return res.response({
      account: accountUpdated,
      message: 'Cuenta Actualizada',
    });
  } catch (error: any) {
    return Boom.badRequest(error.message ?? error);
  }
};
export const remove = async (req: Request, res: ResponseToolkit) => {
  try {
    const params = req.params as unknown as { id: string };
    const { id } = params;
    const account = await findAccountInteractor(id);
    if (!account) return Boom.notFound('No se encontró el banco con servicio');
    return res.response({
      account: await deleteAccountInteractor(account),
      message: 'Eliminado correctamente',
    });
  } catch (error: any) {
    return Boom.badRequest(error.message ?? error);
  }
};
