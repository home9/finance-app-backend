import { Request, ResponseToolkit } from '@hapi/hapi';
import Boom from '@hapi/boom';

import { getClientIp } from '@supercharge/request-ip';
// import { NODE_ENV } from '@/config';

import { registerIpInteractor } from '@core/interactors/userIp';
import { getUserByUidInteractor } from '@/core/interactors/user';
import { UserIpAllow } from '@/core/entities';
import { checkPassword, validateEntity } from '@/internals/utils';

export const registerIp = async (req: Request, res: ResponseToolkit) => {
  try {
    const { pin, device, uid } = req.payload as unknown as {
      pin: string;
      device: string;
      uid: string;
    };
    if (!pin || !device || !uid)
      return Boom.unauthorized('Debe enviar su PIN y dispositivo / usuario');

    const ipClient = getClientIp(req);
    if (!ipClient) return Boom.badData('No se pudo obtener el ip');

    const userFound = await getUserByUidInteractor(uid);
    if (!userFound) return Boom.notFound('No se encontró el usuario');

    if (!(await checkPassword(pin, userFound.pin)))
      return Boom.unauthorized('PIN no válido');

    const userIp = new UserIpAllow();
    userIp.user = userFound;
    userIp.device = device;
    userIp.ip = ipClient;

    const invalidUser = await validateEntity(userIp);
    if (invalidUser) {
      const error = Boom.badRequest(
        'Debe enviar correctamente todos los campos'
      );
      error.reformat();
      error.output.payload.invalid = invalidUser; // Add custom key
      return error;
    }
    return res.response({
      message: 'Registrado correctamente',
      result: { ...(await registerIpInteractor(userIp)) },
    });
  } catch (error: any) {
    return Boom.badRequest(error.message ?? error);
  }
};
