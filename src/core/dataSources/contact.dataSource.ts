import { getRepository } from 'typeorm';

import { Contact, User } from '@core/entities';
import { ContactRepository } from '@core/repository';

export class ContactTypeORM implements ContactRepository {
  async findByNro(user: User, nro: string): Promise<Contact[]> {
    return await getRepository(Contact).find({
      where: { user, nro, isActive: true },
    });
  }
  async findByNroAndCCI(
    user: User,
    nro: string,
    nroCCI: string
  ): Promise<Contact[]> {
    return await getRepository(Contact).find({
      where: [
        { user, nroCCI, isActive: true },
        { user, nro, isActive: true },
      ],
    });
  }
  async register(user: User, contact: Contact): Promise<Contact> {
    contact.user = user;
    return await getRepository(Contact).save(contact);
  }
  async find(user: User, id: string): Promise<Contact | undefined> {
    return await getRepository(Contact).findOne({
      where: { isActive: true, user, id },
    });
  }
  async findByName(user: User, name: string): Promise<Contact | undefined> {
    return await getRepository(Contact).findOne({
      where: { isActive: true, user, name },
    });
  }
  async list(user: User): Promise<[Contact[], number]> {
    return await getRepository(Contact).findAndCount({
      where: { isActive: true, user },
      order: {
        created: 'DESC',
      },
    });
  }
  async update(contact: Contact): Promise<Contact> {
    return await getRepository(Contact).save(contact);
  }
  async remove(contact: Contact): Promise<Contact> {
    contact.isActive = false;
    return await getRepository(Contact).save(contact);
  }
}
