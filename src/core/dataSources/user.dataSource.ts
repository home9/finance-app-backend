import { getRepository } from 'typeorm';

import { UserRepository } from '@core/repository';
import { User } from '@core/entities';
import { Query } from '@internals/shared';

export class UserTypeORM implements UserRepository {
  async createUser(user: User): Promise<User> {
    return await getRepository(User).save(user);
  }

  async deleteUser(id: string): Promise<User> {
    const userDeleted = await this.getUserById(id);
    if (!userDeleted) throw 'El usuario no está registrado';
    userDeleted.isActive = false;
    return await getRepository(User).save(userDeleted);
  }

  async getUserById(id: string): Promise<User | undefined> {
    return await getRepository(User).findOne({
      where: { id, isActive: true },

      select: [
        'id',
        'created',
        'email',
        'fullName',
        'photo',
        'lastLogin',
        'lastIp',
        'plattform',
        'pin',
      ],
    });
  }

  async getUserByUid(uid: string): Promise<User | undefined> {
    return await getRepository(User).findOne({
      where: { uid, isActive: true },
    });
  }

  async getUsers(options: Query): Promise<[User[], number]> {
    const { limit, page } = options;
    const skip = (limit ?? 0) * (page ?? 0);
    const result = await getRepository(User).findAndCount({
      where: { isActive: true },
      take: limit ?? 5,
      skip: skip,
      order: {
        created: 'DESC',
      },
    });
    return result;
  }

  async updateUser(user: User): Promise<User> {
    return await getRepository(User).save(user);
  }
}
