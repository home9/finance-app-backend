import { getRepository, getConnection } from 'typeorm';
import { UserIpAllowRepository } from '@core/repository';
import { User, UserIpAllow } from '../entities';

export class UserIpAlloTypeORM implements UserIpAllowRepository {
  async getUserIpByID(id: string): Promise<UserIpAllow | undefined> {
    return await getRepository(UserIpAllow).findOne({ id, isActive: true });
  }
  //TODO Check
  async removeOther({
    user,
    ip,
  }: {
    user: User;
    ip: string;
  }): Promise<boolean> {
    await getConnection()
      .createQueryBuilder()
      .update(UserIpAllow)
      .set({ isActive: false })
      .where('userId = :id and ip<>:ip', { id: user.id, ip })
      .execute();
    return true;
  }
  async registerIp(userIp: UserIpAllow): Promise<UserIpAllow> {
    const ipAllowFound = await getRepository(UserIpAllow).findOne({
      isActive: true,
      user: userIp.user,
      ip: userIp.ip,
    });
    if (ipAllowFound) throw 'Ya está registrado ese IP en su lista de acceso';
    return await getRepository(UserIpAllow).save(userIp);
  }
  async remove(userIp: UserIpAllow): Promise<UserIpAllow> {
    userIp.isActive = false;
    return await getRepository(UserIpAllow).save(userIp);
  }
  async getUserIp({
    user,
    ip,
  }: {
    user: User;
    ip: string;
  }): Promise<UserIpAllow | undefined> {
    return await getRepository(UserIpAllow).findOne({
      where: {
        user,
        ip,
        isActive: true,
      },
    });
  }
}
