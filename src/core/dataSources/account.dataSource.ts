import { getRepository } from 'typeorm';

import { Account } from '@core/entities';
import { AccountRepository } from '@core/repository';

export class AccountTypeORM implements AccountRepository {
  async registerAccount(account: Account): Promise<Account> {
    return await getRepository(Account).save(account);
  }
  async findAccount(id: string): Promise<Account | undefined> {
    return await getRepository(Account).findOne({
      where: { isActive: true, id },
      relations: [
        'bankToFinancialService',
        'bankToFinancialService.bank',
        'bankToFinancialService.financialServices',
      ],
    });
  }

  async findByNroAndCCIAccount({
    nro,
    nroCCI,
  }: {
    nro: string;
    nroCCI: string;
  }): Promise<Account | undefined> {
    try {
      return await getRepository(Account).findOne({
        where: [
          { nro, isActive: true },
          { nroCCI, isActive: true },
        ],
      });
    } catch (error: any) {
      throw error;
    }
  }

  async listAccount(): Promise<[Account[], number]> {
    return await getRepository(Account).findAndCount({
      where: { isActive: true },
      order: {
        created: 'DESC',
      },
      relations: [
        'bankToFinancialService',
        'bankToFinancialService.bank',
        'bankToFinancialService.financialServices',
      ],
    });
  }
  async updateAccount(account: Account): Promise<Account> {
    return await getRepository(Account).save(account);
  }
  async deleteAccount(account: Account): Promise<Account> {
    account.isActive = false;
    return await getRepository(Account).save(account);
  }
}
