import { getRepository } from 'typeorm';

import { BankToFinancialService } from '@core/entities';
import { BankToFinancialServiceRepository } from '@core/repository';

export class BankToFinancialServiceTypeORM
  implements BankToFinancialServiceRepository {
  async registerBankToFinancialService(
    bankToFinancialService: BankToFinancialService
  ): Promise<BankToFinancialService> {
    return await getRepository(BankToFinancialService).save(
      bankToFinancialService
    );
  }

  async findBankToFinancialService(
    id: string
  ): Promise<BankToFinancialService | undefined> {
    return await getRepository(BankToFinancialService).findOne({
      where: { isActive: true, id },
      relations: ['bank', 'financialServices'],
    });
  }
  async listBankToFinancialService(): Promise<
    [BankToFinancialService[], number]
  > {
    return await getRepository(BankToFinancialService).findAndCount({
      where: { isActive: true },
      order: {
        created: 'DESC',
      },
      relations: ['bank', 'financialServices'],
    });
  }
  async updateBankToFinancialService(
    bankToFinancialService: BankToFinancialService
  ): Promise<BankToFinancialService> {
    return await getRepository(BankToFinancialService).save(
      bankToFinancialService
    );
  }
  async deleteBankToFinancialService(
    bankToFinancialService: BankToFinancialService
  ): Promise<BankToFinancialService> {
    bankToFinancialService.isActive = false;
    return await getRepository(BankToFinancialService).save(
      bankToFinancialService
    );
  }
}
