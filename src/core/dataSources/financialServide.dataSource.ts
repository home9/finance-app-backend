import { getRepository } from 'typeorm';

import { FinancialService } from '@core/entities';
import { FinancialServiceRepository } from '@core/repository';

export class FinancialServiceTypeORM implements FinancialServiceRepository {
  async registerFinancial(
    financialService: FinancialService
  ): Promise<FinancialService> {
    return await getRepository(FinancialService).save(financialService);
  }
  async findFinancial(id: string): Promise<FinancialService | undefined> {
    return await getRepository(FinancialService).findOne({
      where: { isActive: true, id },
    });
  }
  async findByNameFinancial(
    name: string
  ): Promise<FinancialService | undefined> {
    return await getRepository(FinancialService).findOne({
      where: { isActive: true, name },
    });
  }
  async listFinancial(): Promise<[FinancialService[], number]> {
    return await getRepository(FinancialService).findAndCount({
      where: { isActive: true },
      order: {
        created: 'DESC',
      },
    });
  }
  async updateFinancial(
    financialService: FinancialService
  ): Promise<FinancialService> {
    return await getRepository(FinancialService).save(financialService);
  }
  async deleteFinancial(
    financialService: FinancialService
  ): Promise<FinancialService> {
    financialService.isActive = false;
    return await getRepository(FinancialService).save(financialService);
  }
}
