import { getRepository } from 'typeorm';

import { Bank } from '@core/entities';
import { BankRepository } from '@core/repository';

export class BankTypeORM implements BankRepository {
  async findBankByName(name: string): Promise<Bank | undefined> {
    return await getRepository(Bank).findOne({
      where: { isActive: true, name },
    });
  }
  async registerBank(bank: Bank): Promise<Bank> {
    return await getRepository(Bank).save(bank);
  }
  async findBank(id: string): Promise<Bank | undefined> {
    return await getRepository(Bank).findOne({ where: { isActive: true, id } });
  }
  async listBanks(): Promise<[Bank[], number]> {
    return await getRepository(Bank).findAndCount({
      where: { isActive: true },
      order: {
        created: 'DESC',
      },
    });
  }
  async updateBank(bank: Bank): Promise<Bank> {
    return await getRepository(Bank).save(bank);
  }
  async deleteBank(bank: Bank): Promise<Bank> {
    bank.isActive = false;
    return await getRepository(Bank).save(bank);
  }
}
