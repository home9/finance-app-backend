import {
  getUserIp,
  getUserIpByID,
  registerIp,
  remove,
  removeOther,
} from './userIp.interactor';

import { UserIpAlloTypeORM } from '@core/dataSources';

const UserIpAllowRepository = new UserIpAlloTypeORM();

export const getUserIpInteractor = getUserIp(UserIpAllowRepository);
export const getUserIpByIdInteractor = getUserIpByID(UserIpAllowRepository);
export const registerIpInteractor = registerIp(UserIpAllowRepository);
export const removeInteractor = remove(UserIpAllowRepository);
export const removeOtherInteractor = removeOther(UserIpAllowRepository);
