import { User, UserIpAllow } from '@core/entities';
import { UserIpAllowRepository } from '@core/repository';

export const getUserIp =
  (userIpAllowRepository: UserIpAllowRepository) =>
  async ({ user, ip }: { user: User; ip: string }) =>
    await userIpAllowRepository.getUserIp({ user, ip });

export const registerIp =
  (userIpAllowRepository: UserIpAllowRepository) =>
  async (userIp: UserIpAllow) =>
    await userIpAllowRepository.registerIp(userIp);

export const remove =
  (userIpAllowRepository: UserIpAllowRepository) =>
  async (userIp: UserIpAllow) =>
    await userIpAllowRepository.remove(userIp);

export const removeOther =
  (userIpAllowRepository: UserIpAllowRepository) =>
  async ({ user, ip }: { user: User; ip: string }) =>
    await userIpAllowRepository.removeOther({ user, ip });

export const getUserIpByID =
  (userIpAllowRepository: UserIpAllowRepository) => async (id: string) =>
    await userIpAllowRepository.getUserIpByID(id);
