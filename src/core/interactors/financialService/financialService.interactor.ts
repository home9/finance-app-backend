import { FinancialServiceRepository } from '@core/repository';
import { FinancialService } from '@core/entities';

export const registerFinancial = (
  financialRepository: FinancialServiceRepository
) => async (financialService: FinancialService) =>
  await financialRepository.registerFinancial(financialService);

export const findFinancial = (
  financialRepository: FinancialServiceRepository
) => async (id: string) => await financialRepository.findFinancial(id);

export const findByNameFinancial = (
  financialRepository: FinancialServiceRepository
) => async (name: string) =>
  await financialRepository.findByNameFinancial(name);

export const listFinancial = (
  financialRepository: FinancialServiceRepository
) => async () => await financialRepository.listFinancial();

export const updateFinancial = (
  financialRepository: FinancialServiceRepository
) => async (financialService: FinancialService) =>
  await financialRepository.updateFinancial(financialService);

export const deleteFinancial = (
  financialRepository: FinancialServiceRepository
) => async (financialService: FinancialService) =>
  await financialRepository.deleteFinancial(financialService);
