import {
  registerFinancial,
  findFinancial,
  findByNameFinancial,
  listFinancial,
  updateFinancial,
  deleteFinancial,
} from './financialService.interactor';

import { FinancialServiceTypeORM } from '@core/dataSources';

const financialServiceRepository = new FinancialServiceTypeORM();

export const registerFinancialInteractor = registerFinancial(
  financialServiceRepository
);
export const findFinancialInteractor = findFinancial(
  financialServiceRepository
);
export const findByNameFinancialInteractor = findByNameFinancial(
  financialServiceRepository
);
export const listFinancialInteractor = listFinancial(
  financialServiceRepository
);
export const updateFinancialInteractor = updateFinancial(
  financialServiceRepository
);
export const deleteFinancialInteractor = deleteFinancial(
  financialServiceRepository
);
