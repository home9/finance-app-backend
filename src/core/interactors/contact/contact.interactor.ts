import { Contact, User } from '@core/entities';
import { ContactRepository } from '@core/repository';

export const registerContact =
  (contactRepository: ContactRepository) =>
  async ({
    contact,
    user,
  }: {
    contact: Contact;
    user: User;
  }): Promise<Contact> => {
    try {
      const contactFound = contact.nroCCI
        ? await contactRepository.findByNroAndCCI(
            user,
            contact.nro,
            contact.nroCCI
          )
        : await contactRepository.findByNro(user, contact.nro);
      if (contactFound.length > 0)
        throw 'Ya tiene un contacto con nro y/o nroCCI existente';
      return await contactRepository.register(user, contact);
    } catch (error: any) {
      throw error;
    }
  };

export const findContact =
  (contactRepository: ContactRepository) =>
  async ({
    id,
    user,
  }: {
    id: string;
    user: User;
  }): Promise<Contact | undefined> =>
    await contactRepository.find(user, id);

export const findContactByName =
  (contactRepository: ContactRepository) =>
  async ({
    name,
    user,
  }: {
    name: string;
    user: User;
  }): Promise<Contact | undefined> =>
    await contactRepository.findByName(user, name);

export const listContact =
  (contactRepository: ContactRepository) =>
  async ({ user }: { user: User }): Promise<[Contact[], number]> =>
    await contactRepository.list(user);

export const updateContact =
  (contactRepository: ContactRepository) =>
  async (contact: Contact): Promise<Contact> =>
    await contactRepository.update(contact);

export const removeContact =
  (contactRepository: ContactRepository) =>
  async (contact: Contact): Promise<Contact> =>
    await contactRepository.remove(contact);
