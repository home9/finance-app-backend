import {
  findContact,
  findContactByName,
  listContact,
  registerContact,
  removeContact,
  updateContact,
} from './contact.interactor';
import { ContactTypeORM } from '@core/dataSources';

const contactRepository = new ContactTypeORM();

export const findContactInteractor = findContact(contactRepository);

export const findContactByNameInteractor = findContactByName(contactRepository);

export const listContactInteractor = listContact(contactRepository);

export const registerContactInteractor = registerContact(contactRepository);

export const removeContactInteractor = removeContact(contactRepository);

export const updateContactInteractor = updateContact(contactRepository);
