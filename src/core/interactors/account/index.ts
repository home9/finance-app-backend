import {
  registerAccount,
  findAccount,
  listAccount,
  updateAccount,
  deleteAccount,
} from './account.interactor';

import {
  AccountTypeORM,
  BankToFinancialServiceTypeORM,
} from '@core/dataSources';

const accountRepository = new AccountTypeORM();
const bankToFinancialServiceRepository = new BankToFinancialServiceTypeORM();

export const registerAccountInteractor = registerAccount(
  accountRepository,
  bankToFinancialServiceRepository
);

export const findAccountInteractor = findAccount(accountRepository);

export const listAccountInteractor = listAccount(accountRepository);

export const updateAccountInteractor = updateAccount(accountRepository);

export const deleteAccountInteractor = deleteAccount(accountRepository);
