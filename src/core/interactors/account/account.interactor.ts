import {
  AccountRepository,
  BankToFinancialServiceRepository,
} from '@core/repository';
import { Account } from '@core/entities';

export const registerAccount =
  (
    accountRepository: AccountRepository,
    bankToFinancialServiceRepository: BankToFinancialServiceRepository
  ) =>
  async (
    account: Account,
    bankToFinancialServiceId: string
  ): Promise<Account> => {
    try {
      const bankToFinancialService =
        await bankToFinancialServiceRepository.findBankToFinancialService(
          bankToFinancialServiceId
        );
      if (!bankToFinancialService)
        throw 'No existe el banco con entidad fincanciera activa';
      account.bankToFinancialService ??= bankToFinancialService;
      if (
        !(await accountRepository.findByNroAndCCIAccount({
          nro: account.nro,
          nroCCI: account.nroCCI,
        }))
      )
        return accountRepository.registerAccount(account);
      throw 'Ya está registrado ese número de cuenta';
    } catch (error: any) {
      throw error;
    }
  };

export const findAccount =
  (accountRepository: AccountRepository) =>
  async (id: string): Promise<Account | undefined> =>
    accountRepository.findAccount(id);

export const listAccount =
  (accountRepository: AccountRepository) =>
  async (): Promise<[Account[], number]> =>
    accountRepository.listAccount();

export const updateAccount =
  (accountRepository: AccountRepository) =>
  async (account: Account): Promise<Account> =>
    accountRepository.updateAccount(account);

export const deleteAccount =
  (accountRepository: AccountRepository) =>
  async (account: Account): Promise<Account> =>
    accountRepository.deleteAccount(account);
