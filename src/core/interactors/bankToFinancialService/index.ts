import {
  registerBankToFinancialService,
  findBankToFinancialService,
  listBankToFinancialService,
  updateBankToFinancialService,
  deleteBankToFinancialService,
} from './bankToFinancialService.interactor';

import { BankToFinancialServiceTypeORM } from '@core/dataSources';

const bankToFinancialServiceRepository = new BankToFinancialServiceTypeORM();

export const registerBankToFinancialServiceInteractor = registerBankToFinancialService(
  bankToFinancialServiceRepository
);

export const findBankToFinancialServiceInteractor = findBankToFinancialService(
  bankToFinancialServiceRepository
);

export const listBankToFinancialServiceInteractor = listBankToFinancialService(
  bankToFinancialServiceRepository
);

export const updateBankToFinancialServiceInteractor = updateBankToFinancialService(
  bankToFinancialServiceRepository
);

export const deleteBankToFinancialServiceInteractor = deleteBankToFinancialService(
  bankToFinancialServiceRepository
);
