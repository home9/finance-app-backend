import { BankToFinancialServiceRepository } from '@core/repository';
import { BankToFinancialService } from '@core/entities';

export const registerBankToFinancialService = (
  bankToFinancialServiceRepository: BankToFinancialServiceRepository
) => async (bankToFinancialService: BankToFinancialService) =>
  await bankToFinancialServiceRepository.registerBankToFinancialService(
    bankToFinancialService
  );

export const findBankToFinancialService = (
  bankToFinancialServiceRepository: BankToFinancialServiceRepository
) => async (id: string) =>
  await bankToFinancialServiceRepository.findBankToFinancialService(id);

export const listBankToFinancialService = (
  bankToFinancialServiceRepository: BankToFinancialServiceRepository
) => async () =>
  await bankToFinancialServiceRepository.listBankToFinancialService();

export const updateBankToFinancialService = (
  bankToFinancialServiceRepository: BankToFinancialServiceRepository
) => async (bankToFinancialService: BankToFinancialService) =>
  await bankToFinancialServiceRepository.updateBankToFinancialService(
    bankToFinancialService
  );

export const deleteBankToFinancialService = (
  bankToFinancialServiceRepository: BankToFinancialServiceRepository
) => async (bankToFinancialService: BankToFinancialService) =>
  await bankToFinancialServiceRepository.deleteBankToFinancialService(
    bankToFinancialService
  );
