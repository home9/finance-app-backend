import { BankRepository } from '@core/repository';
import { Bank } from '@core/entities';

export const registerBank = (bankRepository: BankRepository) => async (
  bank: Bank
) => await bankRepository.registerBank(bank);

export const findBank = (bankRepository: BankRepository) => async (
  id: string
) => await bankRepository.findBank(id);

export const findBankByName = (bankRepository: BankRepository) => async (
  name: string
) => await bankRepository.findBankByName(name);

export const listBanks = (bankRepository: BankRepository) => async () =>
  await bankRepository.listBanks();

export const updateBank = (bankRepository: BankRepository) => async (
  bank: Bank
) => await bankRepository.updateBank(bank);

export const deleteBank = (bankRepository: BankRepository) => async (
  bank: Bank
) => await bankRepository.deleteBank(bank);
