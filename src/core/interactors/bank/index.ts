import {
  deleteBank,
  findBank,
  listBanks,
  registerBank,
  updateBank,
  findBankByName,
} from './bank.interactor';

import { BankTypeORM } from '@core/dataSources';

const bankRepository = new BankTypeORM();

export const deleteBankInteractor = deleteBank(bankRepository);
export const findBankInteractor = findBank(bankRepository);
export const findBankByNameInteractor = findBankByName(bankRepository);
export const listBanksInteractor = listBanks(bankRepository);
export const registerBankInteractor = registerBank(bankRepository);
export const updateBankInteractor = updateBank(bankRepository);
