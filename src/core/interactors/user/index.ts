import {
  getActiveUsers,
  createUser,
  getUserById,
  getUserByUid,
  deleteUser,
  updateUser,
} from './user.intertactor';
import { UserTypeORM } from '@core/dataSources';

const userRepository = new UserTypeORM();

export const getActiveUsersInteractor = getActiveUsers(userRepository);
export const createUserInteractor = createUser(userRepository);
export const getUserByIdInteractor = getUserById(userRepository);
export const getUserByUidInteractor = getUserByUid(userRepository);
export const deleteUserInteractor = deleteUser(userRepository);
export const updateUserInteractor = updateUser(userRepository);
