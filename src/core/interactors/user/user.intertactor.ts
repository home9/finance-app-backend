import { User } from '@core/entities';
import { UserRepository } from '@core/repository';
import { Query } from '@internals/shared/options';

export const getActiveUsers = (userRepository: UserRepository) => async (
  query: Query
) => await userRepository.getUsers(query);

export const getUserById = (userRepository: UserRepository) => async (
  id: string
) => await userRepository.getUserById(id);

export const getUserByUid = (userRepository: UserRepository) => async (
  uid: string
) => await userRepository.getUserByUid(uid);

export const createUser = (userRepository: UserRepository) => async (
  user: User
) => await userRepository.createUser(user);

export const deleteUser = (userRepository: UserRepository) => async (
  id: string
) => await userRepository.deleteUser(id);

export const updateUser = (userRepository: UserRepository) => async (
  user: User
) => await userRepository.updateUser(user);
