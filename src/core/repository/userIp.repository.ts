import { UserIpAllow, User } from '@core/entities';

export interface UserIpAllowRepository {
  getUserIp({
    user,
    ip,
  }: {
    user: User;
    ip: string;
  }): Promise<UserIpAllow | undefined>;
  getUserIpByID(id: string): Promise<UserIpAllow | undefined>;
  registerIp(userIp: UserIpAllow): Promise<UserIpAllow>;
  remove(userIp: UserIpAllow): Promise<UserIpAllow>;
  removeOther({ user, ip }: { user: User; ip: string }): Promise<boolean>;
}
