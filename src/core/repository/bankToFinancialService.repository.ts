import { BankToFinancialService } from '@core/entities';

export interface BankToFinancialServiceRepository {
  registerBankToFinancialService(
    bankToFinancialService: BankToFinancialService
  ): Promise<BankToFinancialService>;

  findBankToFinancialService(
    id: string
  ): Promise<BankToFinancialService | undefined>;

  listBankToFinancialService(): Promise<[BankToFinancialService[], number]>;

  updateBankToFinancialService(
    bankToFinancialService: BankToFinancialService
  ): Promise<BankToFinancialService>;
  deleteBankToFinancialService(
    bankToFinancialService: BankToFinancialService
  ): Promise<BankToFinancialService>;
}
