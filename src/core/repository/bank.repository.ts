import { Bank } from '@core/entities';

export interface BankRepository {
  registerBank(bank: Bank): Promise<Bank>;
  findBank(id: string): Promise<Bank | undefined>;
  findBankByName(name: string): Promise<Bank | undefined>;
  listBanks(): Promise<[Bank[], number]>;
  updateBank(bank: Bank): Promise<Bank>;
  deleteBank(bank: Bank): Promise<Bank>;
}
