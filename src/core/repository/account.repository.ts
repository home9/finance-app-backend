import { Account } from '@core/entities';

export interface AccountRepository {
  registerAccount(account: Account): Promise<Account>;
  findAccount(id: string): Promise<Account | undefined>;
  findByNroAndCCIAccount({
    nro,
    nroCCI,
  }: {
    nro: string;
    nroCCI: string;
  }): Promise<Account | undefined>;
  listAccount(): Promise<[Account[], number]>;
  updateAccount(account: Account): Promise<Account>;
  deleteAccount(account: Account): Promise<Account>;
}
