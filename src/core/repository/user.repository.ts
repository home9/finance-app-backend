import { User } from '@core/entities';
import { Options, Query } from '@internals/shared';

export interface UserRepository {
  createUser(user: User): Promise<User>;
  deleteUser(id: string): Promise<User>;
  getUserById(id: string, options?: Options): Promise<User | undefined>;
  getUserByUid(uid: string, options?: Options): Promise<User | undefined>;
  getUsers(options?: Query): Promise<[User[], number]>;
  updateUser(user: User, options?: Options): Promise<User>;
}
