import { FinancialService } from '@core/entities';

export interface FinancialServiceRepository {
  registerFinancial(
    financialService: FinancialService
  ): Promise<FinancialService>;
  findFinancial(id: string): Promise<FinancialService | undefined>;
  findByNameFinancial(name: string): Promise<FinancialService | undefined>;
  listFinancial(): Promise<[FinancialService[], number]>;
  updateFinancial(
    financialService: FinancialService
  ): Promise<FinancialService>;
  deleteFinancial(
    financialService: FinancialService
  ): Promise<FinancialService>;
}
