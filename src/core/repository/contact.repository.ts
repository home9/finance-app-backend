import { Contact, User } from '@core/entities';

export interface ContactRepository {
  register(user: User, contact: Contact): Promise<Contact>;
  find(user: User, id: string): Promise<Contact | undefined>;
  findByName(user: User, id: string): Promise<Contact | undefined>;
  findByNro(user: User, nro: string): Promise<Contact[]>;
  findByNroAndCCI(user: User, nro: string, nroCCI: string): Promise<Contact[]>;
  list(user: User): Promise<[Contact[], number]>;
  update(contact: Contact): Promise<Contact>;
  remove(contact: Contact): Promise<Contact>;
}
