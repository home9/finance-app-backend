import { Entity, Column, OneToMany, ManyToOne } from 'typeorm';

import { IsEnum } from 'class-validator';
import { Basic } from './basic';

import { Bank, Account, FinancialService } from '@core/entities';

export enum ECardType {
  credit = 'crédito',
  debit = 'débito',
}

@Entity({ name: 'bank_financial_service' })
export class BankToFinancialService extends Basic {
  @IsEnum(ECardType)
  @Column({ default: ECardType.debit })
  cardType!: ECardType;

  @Column({ default: 1 })
  design!: number;

  @OneToMany(() => Account, (account) => account.bankToFinancialService)
  accounts!: Account[];

  @ManyToOne(() => Bank, (bank) => bank.bankToFinancialServices)
  bank!: Bank;

  @ManyToOne(
    () => FinancialService,
    (financialService) => financialService.bankToFinancialServices
  )
  financialServices!: FinancialService;
}
