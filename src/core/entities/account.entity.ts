import { Entity, Column, ManyToOne, OneToMany } from 'typeorm';
import { NroAccount, NroAccountCCI } from '@internals/utils/validators';
import { ETypeCurrency } from '@constants/typeCurrency';
import { Basic } from './basic';

import { IsEnum, IsNotEmpty, MaxLength, MinLength } from 'class-validator';

import { UserHasAccount, BankToFinancialService } from '@core/entities';

@Entity({ name: 'account' })
export class Account extends Basic {
  @IsNotEmpty({ message: 'Debe enviar un nombre de cuenta' })
  @Column({ default: '' })
  name!: string;

  @Column()
  @NroAccount({ message: 'El número de cuenta debe contener sólo números' })
  @IsNotEmpty({ message: 'Debe enviar un número de cuenta' })
  nro!: string;

  @Column()
  @NroAccountCCI({
    message: 'El número de cuenta cci debe contener sólo números',
  })
  @IsNotEmpty({ message: 'Debe enviar un número de cuenta interbancaria' })
  nroCCI!: string;

  @Column({ default: ETypeCurrency.PEN })
  @IsEnum(ETypeCurrency, {
    message: 'Su servicio financiero no está registrado',
  })
  typeCurrency!: ETypeCurrency;

  @MaxLength(250, {
    message: 'Debe enviar una descripción menor a 250 caracteres',
  })
  @MinLength(10, {
    message: 'Debe enviar una descripción mayor a 10 caracteres',
  })
  @Column({ default: '', length: 250 })
  description!: string;

  @Column('float', { default: 0 })
  amount!: number;

  @ManyToOne(
    () => BankToFinancialService,
    (bankToFinancialService) => bankToFinancialService.accounts
  )
  bankToFinancialService!: BankToFinancialService;

  @OneToMany(() => UserHasAccount, (account) => account.account)
  userHasAccount!: UserHasAccount[];
}
