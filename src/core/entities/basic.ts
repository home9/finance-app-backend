import {
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  PrimaryColumn,
  BeforeInsert,
  AfterLoad,
  AfterInsert,
  AfterUpdate,
  AfterRemove,
} from 'typeorm';

import { v4 as uuidv4 } from 'uuid';

export class Basic {
  @PrimaryColumn({
    type: 'binary',
    nullable: false,
    length: 36,
  })
  id!: Buffer | string;

  @Column({ default: true, type: 'bit', select: false })
  isActive!: boolean;

  @Column()
  @CreateDateColumn({})
  created!: Date;

  @Column()
  @UpdateDateColumn({})
  updated!: Date;

  @BeforeInsert()
  setId() {
    this.id = Buffer.from(uuidv4(), 'utf-8');
  }

  @AfterLoad()
  idAsString() {
    this.id = this.id.toString();
  }

  @AfterInsert()
  idAsStringSave() {
    this.id = this.id.toString();
  }

  @AfterUpdate()
  idAsStringUpdate() {
    this.id = this.id.toString();
  }
  @AfterRemove()
  idAsStringDelete() {
    this.id = this.id.toString();
  }
}
