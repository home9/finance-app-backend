//* Service icon

import { Entity, Column, OneToMany } from 'typeorm';
import { Basic } from './basic';

import { IsNotEmpty } from 'class-validator';
import { Service } from '@core/entities';

@Entity({ name: 'icon' })
export class Icon extends Basic {
  @Column({ default: '' })
  @IsNotEmpty({ message: 'Debe registrar el banco correctamente' })
  name!: string;

  @OneToMany(() => Service, (service) => service.icon)
  services!: Service[];
}
