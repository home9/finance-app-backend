import { Entity, Column, OneToMany, ManyToOne } from 'typeorm';
import { Basic } from './basic';

import { IsNotEmpty } from 'class-validator';

import { ContactTransfer, User } from '@core/entities';
import { NroAccount } from '@/internals/utils/validators';

@Entity({ name: 'contact' })
export class Contact extends Basic {
  @Column({ default: '' })
  @IsNotEmpty({ message: 'Debe enviar un nombre de contacto' })
  name!: string;

  @Column({ default: '' })
  @IsNotEmpty({ message: 'Debe enviar un número de cuenta' })
  @NroAccount({ message: 'El número de cuenta debe contener sólo números' })
  nro!: string;

  @Column({ default: '' })
  nroCCI!: string;

  @OneToMany(() => ContactTransfer, (user) => user.contact)
  contactTransfer!: ContactTransfer[];

  @ManyToOne(() => User, (user) => user.userHasContact)
  user!: User;
}
