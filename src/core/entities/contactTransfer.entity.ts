import { Column, Entity, ManyToOne } from 'typeorm';

import { Basic } from './basic';
import { IsNotEmpty } from 'class-validator';

import { User, Contact } from '@core/entities';

@Entity({ name: 'contact_transfer' })
export class ContactTransfer extends Basic {
  @ManyToOne(() => User, (user) => user.userHasAccount)
  userHasAccount!: User;

  @ManyToOne(() => Contact, (contact) => contact.contactTransfer)
  contact!: Contact;

  @IsNotEmpty({ message: 'Debe enviar una descripción' })
  @Column()
  description!: string;

  @Column('float', { default: 0 })
  amount!: number;
}
