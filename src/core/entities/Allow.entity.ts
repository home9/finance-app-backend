import { Schema } from 'mongoose';

const ObjectId = Schema.Types.ObjectId;
const String = Schema.Types.String;
const Array = Schema.Types.Array;

export const IPAllowDevices = new Schema({
  _id: ObjectId,
  ip: Array,
  user: String,
});

export const SecurityQuestions = new Schema({
  _id: ObjectId,
  answers: Array,
  user: String,
});
