//* Visa, Mastercard, AmercianExpress

import { Entity, Column, OneToMany, AfterUpdate, getRepository } from 'typeorm';
import { Basic } from './basic';

import { IsNotEmpty } from 'class-validator';

import { BankToFinancialService } from '@core/entities';

@Entity({ name: 'financial_service' })
export class FinancialService extends Basic {
  @Column({ default: '' })
  @IsNotEmpty({ message: 'Debe registrar el banco correctamente' })
  name!: string;

  @Column({ default: '' })
  @IsNotEmpty({ message: 'Debe registrar el ícono del servicio' })
  icon!: string;

  @OneToMany(
    () => BankToFinancialService,
    (bankToFinancialService) => bankToFinancialService.financialServices
  )
  bankToFinancialServices!: BankToFinancialService[];

  @AfterUpdate()
  async deleteFinancialService() {
    await getRepository(BankToFinancialService).update(
      { financialServices: this },
      { isActive: false }
    );
  }
}
