import { Column, Entity, ManyToOne } from 'typeorm';

import { Basic } from './basic';
import { IsNotEmpty } from 'class-validator';

import { UserHasAccount, Service } from '@core/entities';

@Entity({ name: 'pay_service' })
export class PayService extends Basic {
  @ManyToOne(() => UserHasAccount, (userHasAccount) => userHasAccount)
  userHasAccount!: UserHasAccount;

  @ManyToOne(() => Service, (service) => service)
  service!: Service;

  @IsNotEmpty({ message: 'Debe enviar una descripción' })
  @Column()
  description!: string;

  @Column('float', { default: 0 })
  amount!: number;
}
