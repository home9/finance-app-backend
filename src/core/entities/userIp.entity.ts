import { Column, Entity, ManyToOne } from 'typeorm';
import { IsNotEmpty } from 'class-validator';
import { Basic } from './basic';

import { User } from '@core/entities';

@Entity({ name: 'user_ip' })
export class UserIpAllow extends Basic {
  @Column()
  @IsNotEmpty({ message: 'Debe enviar su ip' })
  ip!: string;

  @Column()
  @IsNotEmpty({ message: 'Campo no vacío' })
  device!: string;

  @ManyToOne(() => User, (user) => user.userIps)
  user!: User;
}
