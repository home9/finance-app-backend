import { Entity, Column, ManyToOne, OneToMany } from 'typeorm';
import { Basic } from './basic';

import { IsNotEmpty } from 'class-validator';

import { Icon, PayService } from '@core/entities';
@Entity({ name: 'service' })
export class Service extends Basic {
  @Column({ default: '' })
  @IsNotEmpty({ message: 'Debe registrar el banco correctamente' })
  name!: string;

  @ManyToOne(() => Icon, (icon) => icon.services)
  icon!: Icon;

  @OneToMany(() => PayService, (payService) => payService.service)
  payService!: PayService[];
}
