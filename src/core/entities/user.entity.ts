import { Entity, Column, OneToMany } from 'typeorm';

import { IsEmail, IsEnum, Length, IsNumberString } from 'class-validator';
import { TrimLength } from '@internals/utils/validators';
import { DEFAULT_USER_IMAGE } from '@/config';
import { Basic } from './basic';
import { UserHasAccount, Contact, UserIpAllow } from '@core/entities';

export enum EUserRegister {
  email = 'email',
  google = 'google',
  facebook = 'facebook',
}

export enum EOperator {
  claro,
  movistar,
  otro,
}

const operator = {
  'AS6147 Telefonica del Peru S.A.A.': EOperator.movistar,
  'AS12252 America Movil Peru S.A.C.': EOperator.claro,
};

@Entity({ name: 'user' })
export class User extends Basic {
  @Column({ select: false })
  // @IsNotEmpty({ message: 'El uid no debe ser vacío' })
  uid!: string;

  @Column({ default: '' })
  @TrimLength(10, 100, {
    message:
      'Nombre de usuario debe ser mayor a 10 caracteres, menor que 100 y sólo letras',
  })
  fullName!: string;

  @Column()
  @IsEmail()
  email!: string;

  //@Column({ default: '' }) //*Login with email
  //password!: string;

  @Column({ default: false, type: 'boolean', width: 1 })
  emailVerified!: boolean;

  @Column({ default: DEFAULT_USER_IMAGE })
  photo!: string;

  @Column({ default: '' })
  lastLogin!: string;

  @Column({ default: '' })
  lastIp!: string;

  @Column({ default: '' })
  lastCity!: string;

  @Column({ default: '' })
  lastRegion!: string;

  @Column({ default: '', length: 3 })
  lastCountry!: string;

  @Column({})
  @Length(6, 8, { message: 'PIN debe tener 6 dígitos' })
  @IsNumberString({ no_symbols: true })
  pin!: string;

  @Column({ default: EUserRegister.email })
  @IsEnum(EUserRegister, { message: 'Plataforma no válida' })
  plattform!: EUserRegister;

  @OneToMany(() => UserHasAccount, (user) => user.owner)
  ownerHasAccount!: UserHasAccount[];

  @OneToMany(() => UserHasAccount, (user) => user.user)
  userHasAccount!: UserHasAccount[];

  @OneToMany(() => Contact, (contact) => contact.user)
  userHasContact!: Contact[];

  @OneToMany(() => UserIpAllow, (userIpAllow) => userIpAllow.user)
  userIps!: UserIpAllow[];
}
