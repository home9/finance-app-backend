import { Entity, Column, OneToMany, AfterUpdate, getRepository } from 'typeorm';
import { Basic } from './basic';

import { IsNotEmpty } from 'class-validator';
import { BankToFinancialService } from '@core/entities';

@Entity({ name: 'bank' })
export class Bank extends Basic {
  @Column({})
  @IsNotEmpty({ message: 'Debe registrar el banco correctamente' })
  name!: string;

  @Column({ default: true })
  isBank!: boolean;

  @OneToMany(
    () => BankToFinancialService,
    (bankToFinancialService) => bankToFinancialService.bank
  )
  bankToFinancialServices!: BankToFinancialService[];

  @AfterUpdate()
  async deleteFinancialService() {
    await getRepository(BankToFinancialService).update(
      { bank: this },
      { isActive: false }
    );
  }
}
