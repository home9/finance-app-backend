import { Entity, ManyToOne, OneToMany } from 'typeorm';

import { Basic } from './basic';

import { User, Account, PayService, ContactTransfer } from '@core/entities';

@Entity({ name: 'user_has_account' })
export class UserHasAccount extends Basic {
  @ManyToOne(() => User, (owner) => owner.ownerHasAccount)
  owner!: User;

  @ManyToOne(() => User, (user) => user.userHasAccount)
  user!: User;

  @ManyToOne(() => Account, (account) => account.userHasAccount)
  account!: Account;

  @OneToMany(() => PayService, (payService) => payService.userHasAccount)
  payService!: PayService[];

  @OneToMany(
    () => ContactTransfer,
    (contactTransfer) => contactTransfer.userHasAccount
  )
  ContactTransfers!: ContactTransfer[];
}
