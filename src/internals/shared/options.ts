type QueryUser = {
  limit?: number;
  page?: number;
  search?: string;
  order?: string;
};

export type ParamsUser = {
  id: string;
  uid?: string;
};

export type Query = QueryUser;
export type Params = ParamsUser;

export type Options = Query & Params;
