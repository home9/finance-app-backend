import { Request } from '@hapi/hapi';
import { Token } from '../utils';

export class GlobalUser {
  public static async getUser(req: Request): Promise<any> {
    try {
      let { authorization } = req.headers;
      if (!authorization) throw 'Debe iniciar sesión';
      authorization = authorization.split(' ')[1];
      const token = new Token();
      token.token = authorization;
      return await token.decode();
    } catch (error: any) {
      throw error;
    }
  }
}
