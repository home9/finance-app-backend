import dayjs, { Dayjs } from 'dayjs';
import utc from 'dayjs/plugin/utc';

export class Date {
  private _date: Dayjs;

  static init() {
    dayjs.extend(utc);
  }

  constructor() {
    this._date = dayjs().utc();
  }

  nowUTC(formatDate = '') {
    return this._date.format(formatDate);
  }
}
