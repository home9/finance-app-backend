export function DeepBuffer(obj: any) {
  for (let prop in obj) {
    if (obj.hasOwnProperty(prop)) {
      if (Buffer.isBuffer(obj[prop])) {
        obj[prop] = obj[prop].toString();
      } else if (typeof obj[prop] == 'object') {
        DeepBuffer(obj[prop]);
      }
    }
  }
}
