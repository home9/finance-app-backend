import {
  registerDecorator,
  ValidationOptions,
  ValidatorConstraint,
  ValidatorConstraintInterface,
  ValidationArguments,
} from 'class-validator';

@ValidatorConstraint({ async: false })
export class NroAccountCCIConstraint implements ValidatorConstraintInterface {
  validate(value: string, _: ValidationArguments) {
    return /^[0-9-]{12,30}$/.test(value);
  }
}

export function NroAccountCCI(validationOptions?: ValidationOptions) {
  return function (object: Object, propertyName: string) {
    registerDecorator({
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      constraints: [],
      validator: NroAccountCCIConstraint,
    });
  };
}
