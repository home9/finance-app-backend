import {
  registerDecorator,
  ValidationOptions,
  ValidatorConstraint,
  ValidatorConstraintInterface,
  ValidationArguments,
} from 'class-validator';

@ValidatorConstraint({ async: false })
export class NroAccountConstraint implements ValidatorConstraintInterface {
  validate(value: string, _: ValidationArguments) {
    return /^([0-9-]{14,30})$/.test(value);
  }
}

export function NroAccount(validationOptions?: ValidationOptions) {
  return function (object: Object, propertyName: string) {
    registerDecorator({
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      constraints: [],
      validator: NroAccountConstraint,
    });
  };
}
