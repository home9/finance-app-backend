import { validate } from 'class-validator';

export const validateEntity = async (
  objectToValidate: object
): Promise<
  | false
  | {
      message: string;
      problem: {
        constraints:
          | {
              [type: string]: string;
            }
          | undefined;
      }[];
    }
> => {
  const valid = await validate(objectToValidate);
  if (valid.length === 0) return false;
  return {
    message: 'Error',
    problem: valid.map(({ constraints }) => {
      return { constraints };
    }),
  };
};
