import {
  registerDecorator,
  ValidationOptions,
  ValidatorConstraint,
  ValidatorConstraintInterface,
  ValidationArguments,
} from 'class-validator';

@ValidatorConstraint({ async: false })
export class TrimLengthConstraint implements ValidatorConstraintInterface {
  validate(value: string, args: ValidationArguments) {
    const trim = value.trim();
    return (
      value === trim &&
      /^([a-zA-ZñÑáéíóúÁÉÍÓÚü ]+)$/.test(trim) &&
      trim.length >= args.constraints[0] &&
      trim.length <= args.constraints[1]
    );
  }
}

export function TrimLength(
  property?: number,
  property2?: number,
  validationOptions?: ValidationOptions
) {
  return function (object: Object, propertyName: string) {
    registerDecorator({
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      constraints: [property, property2],
      validator: TrimLengthConstraint,
    });
  };
}
