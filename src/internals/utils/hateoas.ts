export const hateoas = <Entity>(
  result: Entity[],
  {
    limit: limitQuery,
    page: pageQuery,
    count = 10,
  }: {
    count: number | undefined;
    limit: number | undefined;
    page: number | undefined;
  }
): {
  result: Entity[];
  next: string;
  previous: string;
  first: string;
  last: string;
} => {
  const page = pageQuery ?? 0;
  const limit = limitQuery ?? 10;
  const pageCount = Math.round(count / limit);

  return {
    result,
    next: `page=${
      pageCount === 0
        ? 0
        : page > 0
        ? pageCount - 1 < page + 1
          ? pageCount - 1
          : page + 1
        : 0
    }`,
    previous: `page=${
      pageCount > 0
        ? page > 0
          ? pageCount - 1 < page - 1
            ? pageCount - 1
            : page - 1
          : 0
        : 0
    }`,
    first: 'page=0',
    last: `page=${pageCount > 0 ? pageCount - 1 : 0}`,
  };
};
