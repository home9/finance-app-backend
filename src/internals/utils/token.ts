import { SECRET_TOKEN, TOKEN_LIMIT } from '@/config';
import jwt from 'jsonwebtoken';

export class Token {
  private _token: string = '';

  encode(payload: any = {}) {
    this._token = jwt.sign({ ...payload }, SECRET_TOKEN, {
      expiresIn: `${TOKEN_LIMIT}m`,
    });
  }

  refresh(payload: any = {}) {
    this._token = jwt.sign({ ...payload }, SECRET_TOKEN, {
      expiresIn: `${TOKEN_LIMIT}m`,
    });
  }

  async decode<Payload>(): Promise<Payload> {
    return new Promise((resolve, reject) => {
      jwt.verify(this._token, SECRET_TOKEN, (error, decoded: any) => {
        if (error) reject({ message: 'No se pudo decodificar' });
        if (decoded) resolve(decoded as Payload);
        reject({ message: 'No decoded' });
      });
    });
  }

  get token() {
    return this._token;
  }

  set token(token: string) {
    this._token = token;
  }
}
