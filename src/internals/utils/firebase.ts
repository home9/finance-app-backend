import * as admin from 'firebase-admin';
import path from 'path';
import { User, EUserRegister } from '@core/entities';
import { encrypt } from './encrypt';
import {
  DEFAULT_USER_IMAGE,
  FIREBASE_CREDENTIAL,
  FIREBASE_DATABASEURL,
  FIREBASE_PROJECTID,
  FIREBASE_STORAGEBUCKET,
} from '@/config';

export enum EFirebaseProvider {
  google = 'google.com',
  facebook = 'facebook.com',
  password = 'password',
}

export type UserDecoded = {
  uid: string | undefined;
  email: string | undefined;
  emailVerified: boolean | undefined;
  photoURL: string | undefined;
  providerData: [{ providerId?: EFirebaseProvider }] | undefined;
  displayName: string | undefined;
};

export class Firebase {
  private _authentication!: admin.auth.Auth;
  private _userid!: string;

  constructor() {
    this._authentication = admin.auth();
  }

  public static init() {
    try {
      admin.initializeApp({
        credential: admin.credential.cert(
          path.join(__dirname, '../../..', FIREBASE_CREDENTIAL)
        ),
        databaseURL: FIREBASE_DATABASEURL,
        projectId: FIREBASE_PROJECTID,
        storageBucket: FIREBASE_STORAGEBUCKET,
      });
    } catch (error: any) {
      throw new Error(error);
    }
  }

  public async getUserData(): Promise<UserDecoded> {
    try {
      const userFirebase = await this.user;
      const decoded = userFirebase.toJSON() as UserDecoded;
      const user: UserDecoded = {
        uid: decoded?.uid,
        email: decoded?.email,
        emailVerified: decoded?.emailVerified,
        photoURL: decoded?.photoURL,
        providerData: decoded?.providerData,
        displayName: decoded?.displayName,
      };
      return user;
    } catch (error: any) {
      throw error;
    }
  }

  public async getUserFromFirebase({
    fullName,
  }: {
    fullName: string | undefined;
  }): Promise<User | false> {
    const userFirebase = await this.getUserData();
    const user: User = new User();
    user.uid = userFirebase.uid ?? '';
    user.email = userFirebase.email ?? '';
    user.fullName = userFirebase.displayName ?? fullName ?? 'Usuario';
    if (!userFirebase.providerData) return false;

    const providerId = userFirebase.providerData?.[0]?.providerId;
    switch (providerId) {
      case EFirebaseProvider.google:
        user.plattform = EUserRegister.google;
        break;
      case EFirebaseProvider.facebook:
        user.plattform = EUserRegister.facebook;
        break;

      default:
        user.plattform = EUserRegister.email;
        // if (!password) return false;
        // user.password = await encrypt(password);
        break;
    }
    user.photo = userFirebase.photoURL ?? DEFAULT_USER_IMAGE;
    user.emailVerified = userFirebase.emailVerified ?? false;
    return user;
  }

  public get auth(): admin.auth.Auth {
    return this._authentication;
  }

  public get userid(): string {
    return this._userid;
  }
  public set userid(value: string) {
    this._userid = value;
  }

  public get user() {
    return this._authentication.getUser(this._userid);
  }
}
