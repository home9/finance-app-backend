import nodemailer from 'nodemailer';
import { EMAIL_USER, EMAIL_PASSWORD } from '@/config';

export async function email({
  to,
  ip,
  city,
  region,
  country,
}: {
  to: string;
  ip?: string;
  city?: string;
  region?: string;
  country?: string;
}) {
  // Generate test SMTP service account from ethereal.email
  // Only needed if you don't have a real mail account for testing
  // let testAccount = await nodemailer.createTestAccount();

  // create reusable transporter object using the default SMTP transport
  let transporter = nodemailer.createTransport({
    host: 'mail.privateemail.com',
    //service: 'gmail',
    port: 465,
    secure: true, // true for 465, false for other ports
    auth: {
      user: EMAIL_USER, // generated ethereal user
      pass: EMAIL_PASSWORD, // generated ethereal password
    },
  });

  // send mail with defined transport object
  // let info = await transporter.sendMail({
  await transporter.sendMail({
    from: '"Finance APP 💰" <support@jsiapo.dev> ', // sender address
    to, // list of receivers
    subject: 'Nuevo inicio de sesión', // Subject line
    priority: 'high',
    // text: 'Se detectó un nuevo inicio de sesión para', // plain text body
    html: `<div style=\"font-size: 145%\"><h1 style=\"text-align: center\">Nuevo inicio de sesión detectado</h1><br /><b>IP: </b>${ip}<br /><b>Ubicación: </b>${city}<br /><b>Región: </b>${region}<br /><b>País: </b>${country}</div>`, // html body
  });

  // console.log('Message sent: %s', info.messageId);
  // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>

  // Preview only available when sending through an Ethereal account
  // console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
  // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
}
