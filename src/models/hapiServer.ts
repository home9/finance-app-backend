import { Server, ResponseToolkit } from '@hapi/hapi';
import Boom from '@hapi/boom';
import hapiAuthJwt2 from 'hapi-auth-jwt2';
import { routes } from '@/routes';
import { PORT, NODE_ENV, SECRET_TOKEN, IP_TOKEN } from '@/config';

import { getUserIpByIdInteractor } from '@/core/interactors/userIp';
import { getUserByIdInteractor } from '@/core/interactors/user';
import { Firebase, Date, DeepTrim, Token } from '@internals/utils';

export class HapiServer {
  private _server!: Server;

  constructor() {
    this._server = new Server({
      port: PORT,
      host: '0.0.0.0',
      debug: { log: ['error'] },
    });
    Firebase.init();
    Date.init();
  }

  private async Plugins() {
    await this._server.register(hapiAuthJwt2);
    this._server.auth.strategy('jwt', 'jwt', {
      key: SECRET_TOKEN,
      validate: (_, req, __) => {
        req.auth.isAuthorized = true;
        return { isValid: true };
      },
      verifyOptions: {
        ignoreExpiration: NODE_ENV !== 'PRODUCTION',
      },
    });
    this._server.auth.default('jwt');
    await this._server.register({
      plugin: require('hapi-geo-locate'),
      options: {
        enabledByDefault: false,
        authToken: IP_TOKEN,
      },
    });
    this._server.ext({
      type: 'onRequest',
      method: (req, res) => {
        DeepTrim(req.query);
        return res.continue;
      },
    });
    this._server.ext({
      type: 'onPreHandler',
      method: (req, res) => {
        DeepTrim(req.payload);
        DeepTrim(req.params);
        return res.continue;
      },
    });
    this._server.ext({
      type: 'onPreResponse',
      method: async (req, res) => {
        try {
          if ((req.response as any).isBoom) return res.continue;
          const credentials = res.request.auth.credentials;
          delete credentials.iat;
          delete credentials.exp;

          const { ip, id } = credentials as unknown as {
            ip: string;
            id: string;
          };
          const user = await getUserByIdInteractor(id);
          if (!user) return Boom.notFound('Usuario no registrado');
          const userIp = await getUserIpByIdInteractor(ip);

          if (!userIp) return Boom.unauthorized('Dirección IP no registrada');

          const result = (req.response as any).request.response.source;
          if (result.token) return res.continue; //* Login send token from user.controller

          const token = new Token();
          token.refresh(credentials);

          return res.response({
            ...result,
            token: token.token,
          });
        } catch (error: any) {
          return res.continue;
        }
      },
    });

    this._server.route({
      method: 'GET',
      path: '/',
      handler: (_, reply: ResponseToolkit) => {
        return reply.response(`<h1>Server ${NODE_ENV} running 🚀</h1>`);
      },
      options: {
        auth: false,
      },
    });
  }

  private Routes() {
    routes(this._server, '/api/v1');

    this._server.route({
      method: 'GET',
      path: '/api/location',
      handler: (request: { location: any }) => {
        const location = request.location;
        return location;
      },
      options: {
        auth: false,
        plugins: {
          'hapi-geo-locate': {
            enabled: true,
          },
        },
      },
    });
    this._server.route({
      method: '*',
      path: '/{any*}',
      handler: function (_, res) {
        return res.response({ message: 'Page not found' }).code(404);
      },
      options: {
        auth: false,
      },
    });
  }

  public async start() {
    await this.Plugins();
    this.Routes();
    await this._server.start();
    console.log(`Server ${NODE_ENV} running in ${this._server.info.uri} 🚀`);
  }
}
