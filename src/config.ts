import { ConnectionOptions } from 'typeorm';

require('dotenv').config();
export const PORT = parseInt(process.env.PORT || '3001');
export const NODE_ENV = process.env.NODE_ENV || 'test';

export const DB_USERNAME = process.env.DB_USERNAME || '';
export const DB_DATABASE = process.env.DB_DATABASE || '';
export const DB_PASSWORD = process.env.DB_PASSWORD || '';
export const DB_SERVER = process.env.DB_SERVER || '';
export const DB_PORT = parseInt(process.env.DB_PORT || '3306');
export const SALT = parseInt(process.env.SALT || '11');
export const SECRET_TOKEN = process.env.SECRET_TOKEN || 'secret';
export const TOKEN_LIMIT = process.env.TOKEN_LIMIT || '8';
export const FIREBASE_DATABASEURL =
  `https://${process.env.FIREBASE_DATABASEURL}` || ``;
export const FIREBASE_PROJECTID = process.env.FIREBASE_PROJECTID || ``;
export const FIREBASE_STORAGEBUCKET = process.env.FIREBASE_STORAGEBUCKET || ``;
export const FIREBASE_CREDENTIAL = process.env.FIREBASE_CREDENTIAL || ``;
export const DEFAULT_USER_IMAGE = process.env.DEFAULT_USER_IMAGE || ``;
export const TABLEPREFIX = process.env.TABLEPREFIX || `tbl_`; //! Important send
export const IP_TOKEN = process.env.IP_TOKEN || ``; //! Important send
export const EMAIL_USER = process.env.EMAIL_USER || ``; //! Important send
export const EMAIL_PASSWORD = process.env.EMAIL_PASSWORD || ``; //! Important send

export const dbOptions: ConnectionOptions = {
  type: 'mysql',
  host: DB_SERVER,
  port: DB_PORT,
  username: DB_USERNAME,
  password: DB_PASSWORD,
  database: DB_DATABASE,
  synchronize: true,
  logging: false,
  entities: ['dist/core/entities/**/*.entity.js'],
  entityPrefix: `${TABLEPREFIX}_`,
  charset: 'utf8mb4_spanish2_ci',
  cache: {
    type: 'redis',
    options: {
      host: 'localhost',
    },
  },
};
