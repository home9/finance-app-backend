import { Server } from '@hapi/hapi';
import {
  register,
  list,
  find,
  update,
  remove,
} from '@controllers/bankToFinancialService.controller';

export default (server: Server, prefix: string) => {
  server.route({
    method: 'POST',
    path: `${prefix}/bank_financial_service`,
    handler: register,
  });
  server.route({
    method: 'GET',
    path: `${prefix}/bank_financial_service`,
    handler: list,
  });

  server.route({
    method: 'GET',
    path: `${prefix}/bank_financial_service/{id}`,
    handler: find,
  });

  server.route({
    method: 'PATCH',
    path: `${prefix}/bank_financial_service/{id}`,
    handler: update,
  });

  server.route({
    method: 'DELETE',
    path: `${prefix}/bank_financial_service/{id}`,
    handler: remove,
  });
};
