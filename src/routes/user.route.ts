import {
  createUser,
  deleteUser,
  getUserById,
  getUsers,
  udpateUser,
  login,
  isUserRegister,
} from '@/controllers/user.controller';
import { Server } from '@hapi/hapi';

export default (server: Server, prefix: string) => {
  server.ext({
    type: 'onRequest',
    method: (req, res) => {
      if (req.path.match(/(\/api\/v1\/user)/i)) {
        req.query.limit = req.query.limit ? +req.query.limit : undefined;
        req.query.page = req.query.page ? +req.query.page : undefined;
      }
      return res.continue;
    },
  });

  server.route({
    method: 'POST',
    path: `${prefix}/user/isRegistered`,
    handler: isUserRegister,
    options: {
      auth: false,
    },
  });

  server.route({
    method: 'POST',
    path: `${prefix}/user/login`,
    handler: login,
    options: {
      auth: false,
      plugins: {
        'hapi-geo-locate': {
          enabled: true,
        },
      },
    },
  });

  server.route({
    method: 'POST',
    path: `${prefix}/user`,
    handler: createUser,
    options: {
      description: 'Creación de nuevo usuario',
      notes: 'Crear usuario con hapi.js',
      tags: ['user', 'create'],
      auth: false,
      plugins: {
        'hapi-geo-locate': {
          enabled: true,
        },
      },
    },
  });

  server.route({
    method: 'PATCH',
    path: `${prefix}/user/{id}`,
    handler: udpateUser,
    options: {
      description: 'Actualización de datos usuario',
      notes: 'Actualizar usuario con hapi.js',
      tags: ['user', 'create'],
    },
  });

  server.route({
    method: 'GET',
    path: `${prefix}/user`,
    handler: getUsers,
    options: {
      auth: {
        scope: ['user'],
      },
    },
  });

  server.route({
    method: 'GET',
    path: `${prefix}/user/{id}`,
    handler: getUserById,
  });
  server.route({
    method: 'DELETE',
    path: `${prefix}/user/{id}`,
    handler: deleteUser,
  });
};
