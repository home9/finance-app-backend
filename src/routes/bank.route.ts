import { Server } from '@hapi/hapi';
import {
  registerBank,
  listBank,
  findBank,
  deleteBank,
  updateBank,
} from '@/controllers/bank.controller';

export default (server: Server, prefix: string) => {
  server.ext('onRequest', (req, res) => {
    if (req.path.match(/(\/api\/v1\/bank)/i)) {
    }
    return res.continue;
  });

  server.route({
    method: 'GET',
    path: `${prefix}/bank`,
    handler: listBank,
  });

  server.route({
    method: 'GET',
    path: `${prefix}/bank/{id}`,
    handler: findBank,
  });

  server.route({
    method: 'POST',
    path: `${prefix}/bank`,
    handler: registerBank,
  });

  server.route({
    method: 'PATCH',
    path: `${prefix}/bank/{id}`,
    handler: updateBank,
  });

  server.route({
    method: 'DELETE',
    path: `${prefix}/bank/{id}`,
    handler: deleteBank,
  });
};
