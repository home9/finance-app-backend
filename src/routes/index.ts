import { Server } from '@hapi/hapi';

import { default as UserRoute } from './user.route';
import { default as BankRoute } from './bank.route';
import { default as FinancialServiceRoute } from './financialService.route';
import { default as BankToFinancialServiceRoute } from './bankToFinancialService.route';
import { default as AccountRoute } from './account.route';
import { default as ContactRoute } from './contact.route';
import { default as UserIpRoute } from './userIp.route';

export const routes = (server: Server, version: string) => {
  UserRoute(server, version);
  BankRoute(server, version);
  FinancialServiceRoute(server, version);
  BankToFinancialServiceRoute(server, version);
  AccountRoute(server, version);
  ContactRoute(server, version);
  UserIpRoute(server, version);
};
