import { Server } from '@hapi/hapi';
import {
  register,
  list,
  find,
  update,
  remove,
} from '@controllers/account.controller';

export default (server: Server, prefix: string) => {
  server.ext({
    type: 'onPreHandler',
    method: (req, res) => {
      //TODO Fix Payload
      return res.continue;
    },
  });

  server.route({
    method: 'POST',
    path: `${prefix}/account`,
    handler: register,
  });

  server.route({
    method: 'GET',
    path: `${prefix}/account`,
    handler: list,
  });

  server.route({
    method: 'GET',
    path: `${prefix}/account/{id}`,
    handler: find,
  });

  server.route({
    method: 'PATCH',
    path: `${prefix}/account/{id}`,
    handler: update,
  });

  server.route({
    method: 'DELETE',
    path: `${prefix}/account/{id}`,
    handler: remove,
  });
};
