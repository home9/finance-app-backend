import { Server } from '@hapi/hapi';
import {
  list,
  find,
  register,
  remove,
  update,
} from '@controllers/contact.controller';

export default (server: Server, prefix: string) => {
  server.ext({
    type: 'onPreHandler',
    method: (req, res) => {
      //TODO Fix Payload
      return res.continue;
    },
  });

  server.route({
    method: 'POST',
    path: `${prefix}/contact`,
    handler: register,
  });

  server.route({
    method: 'GET',
    path: `${prefix}/contact`,
    handler: list,
  });

  server.route({
    method: 'GET',
    path: `${prefix}/contact/{id}`,
    handler: find,
  });

  server.route({
    method: 'PATCH',
    path: `${prefix}/contact/{id}`,
    handler: update,
  });

  server.route({
    method: 'DELETE',
    path: `${prefix}/contact/{id}`,
    handler: remove,
  });
};
