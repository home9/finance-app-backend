import { Server } from '@hapi/hapi';
import { registerIp } from '@controllers/userIp.controller';

export default (server: Server, prefix: string) => {
  server.route({
    method: 'POST',
    path: `${prefix}/device`,
    handler: registerIp,
    options: {
      auth: false,
    },
  });
  // server.route({
  //   method: 'GET',
  //   path: `${prefix}/ip`,
  //   handler: list,
  // });
  // server.route({
  //   method: 'GET',
  //   path: `${prefix}/ip/{id}`,
  //   handler: find,
  // });

  // server.route({
  //   method: 'PATCH',
  //   path: `${prefix}/ip/{id}`,
  //   handler: update,
  // });
  // server.route({
  //   method: 'DELETE',
  //   path: `${prefix}/ip/{id}`,
  //   handler: remove,
  // });
};
