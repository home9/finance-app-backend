import 'reflect-metadata';
import 'module-alias/register';

import { Connection, createConnection } from 'typeorm';
import { HapiServer } from '@/models/hapiServer';
import { dbOptions } from '@/config';

const hapiServer = new HapiServer();

createConnection(dbOptions)
  .then(async (_: Connection) => hapiServer.start())
  .catch((error) => console.log(error));
